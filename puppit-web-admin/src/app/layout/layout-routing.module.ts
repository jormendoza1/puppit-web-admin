import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdopcionListadoComponent } from './adopcion-listado/adopcion-listado.component';
import { ConfiguracionCuentaComponent } from './configuracion-cuenta/configuracion-cuenta.component';
import { LoginComponent } from './login/login.component';
import { MascotaNuevoComponent } from './mascota-nuevo/mascota-nuevo.component';

const routes: Routes = [
  {
    path: 'mascota',
    component: MascotaNuevoComponent
  },
  {
    path: 'adopcion-listado',
    component: AdopcionListadoComponent
  },
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'configuracion-cuenta',
    component: ConfiguracionCuentaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
