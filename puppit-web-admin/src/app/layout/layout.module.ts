import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MascotaNuevoComponent } from './mascota-nuevo/mascota-nuevo.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MascotasService } from '../services/mascotas.service';
import { AdopcionListadoComponent } from './adopcion-listado/adopcion-listado.component';
import { LoginComponent } from './login/login.component';
import { ConfiguracionCuentaComponent } from './configuracion-cuenta/configuracion-cuenta.component';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    MascotaNuevoComponent,
    AdopcionListadoComponent,
    LoginComponent,
    ConfiguracionCuentaComponent
  ],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    MascotasService,
  ]
})
export class LayoutModule { }
