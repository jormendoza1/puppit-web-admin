import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdopcionListadoComponent } from './adopcion-listado.component';

describe('AdopcionListadoComponent', () => {
  let component: AdopcionListadoComponent;
  let fixture: ComponentFixture<AdopcionListadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdopcionListadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdopcionListadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
