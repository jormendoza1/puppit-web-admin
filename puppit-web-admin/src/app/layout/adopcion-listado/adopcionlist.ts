export class AdopcionList{
    id : number;
    fecha : Date;
    name : string;
    nacimiento : string;
    edadAproximada: string;
    url_imagen : string;
    apellido : string;
    nombre : string;
    ong : string;

    constructor(public i:number, fec:Date, na:string, nac: string, eA: string, url:string, ap:string, nom:string, o:string){
        this.id = i;
        this.fecha = fec;
        this.name = na;
        this.nacimiento= nac;
        this.edadAproximada= eA;
        this.url_imagen = url; 
        this.apellido = ap;
        this.nombre = nom;
        this.ong = o;
    }
}