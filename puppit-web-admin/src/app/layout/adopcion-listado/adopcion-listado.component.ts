import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { AdopcionesService } from 'src/app/services/adopciones.service';
import { AdoptantesService } from 'src/app/services/adoptantes.service';
import { MascotasService } from 'src/app/services/mascotas.service';
import { OngService } from 'src/app/services/ong.service';
import { Adopcion } from './adopcion';
import { AdopcionList } from './adopcionlist';
import { Adoptante } from './adoptante';
import { Mascota } from './mascota';
import { Ong } from './ong';

@Component({
  selector: 'app-adopcion-listado',
  templateUrl: './adopcion-listado.component.html',
  styleUrls: ['./adopcion-listado.component.css']
})
export class AdopcionListadoComponent implements OnInit {
  adopciones : Array<AdopcionList> = [];
  ongs : Array<Ong> = [];
  mascotas : Array<Mascota> = [];
  adoptantes : Array<Adoptante> = [];
  adopcion = new Adopcion(0,new Date(),0,0,0,0);
  mascota = new Mascota(0,"","",0,"",0.0,"",new Date(),"","","",0, false);
  adoptante = new Adoptante(0,"","","","","","","",new Date(),"");
  ongNombre : string = "";
  mascotaSexo : string= "";
  adoptanteGenero : string="";
  admin : number = 0;

  pipe = new DatePipe('en-US');
  fechaHoy = this.pipe.transform(Date.now(), 'yyyy/MM/dd');
  fechaNacimiento: any;
  edadAproximada: any;

  constructor(private adopcionesService: AdopcionesService, private mascotasService: MascotasService, private ongService: OngService, private adoptantesService: AdoptantesService) { }

  ngOnInit(): void {
    var aux = localStorage.getItem('currentUser');
    this.admin = Number(aux);
    
    if(this.admin==1){
      this.cargarAdopciones();
      this.cargarONGs();
    }else{
      this.cargarAdopcionesxONG();
      this.cargarMascotas();
      this.cargarAdoptantes();
    }
  }

  public cargarAdopciones(){
    this.adopcionesService.getAdopciones().subscribe( resp => {
      this.adopciones = resp.rows;
      
      this.calculoEdadAproximada(this.adopciones);
    });
  }

  public cargarAdopcionesxONG(){
    this.ongService.getONGByUserId(this.admin).subscribe( resp => {
      this.filtrarPorONG(resp.rows[0].id);
    });
  }

  public cargarONGs(){
    this.ongService.getONGs().subscribe( resp => {
      this.ongs = resp.rows;
    });
  }

  public cargarMascotas(){
    this.ongService.getONGByUserId(this.admin).subscribe( resp => {
      this.mascotasService.getMascotasAdoptadasByONG(resp.rows[0].id).subscribe( resp => {
        this.mascotas = resp.rows;
      });
    });    
  }

  public cargarAdoptantes(){
    this.ongService.getONGByUserId(this.admin).subscribe( resp => {
      this.adoptantesService.getAdoptantesByONG(resp.rows[0].id).subscribe( resp => {
        this.adoptantes = resp.rows;
      });
    });
  }

  public showModal(adopcionId:number, ongNombre:string){
    this.ongNombre = ongNombre;

    //Recupera datos de adopcion seleccionada
    this.adopcionesService.getAdopcionById(adopcionId).subscribe( resp => {
      this.adopcion = resp.rows[0];

      //Recupera datos de mascota adoptada
      this.mascotasService.getMascotaById(resp.rows[0].id_mascota).subscribe( resp => {
        this.mascota = resp.rows[0];

        this.fechaNacimiento = this.pipe.transform(this.mascota.nacimiento, 'yyyy/MM/dd');
        this.calcularEdadAproximada(this.fechaNacimiento);

        if(resp.rows[0].sexo == 'M'||resp.rows[0].sexo == 'm'){
          this.mascotaSexo = "Macho";
        }else{
          this.mascotaSexo = "Hembra";
        }
      });

      //Recupera datos de adoptante
      this.adoptantesService.getAdoptanteById(resp.rows[0].id_adoptante).subscribe( resp => {
        this.adoptante = resp.rows[0];

        if(resp.rows[0].sexo == 'M'){
          this.adoptanteGenero = "Mujer";
        }else{
          this.adoptanteGenero = "Hombre";
        }
      });
    });   
  }

  public filtrarPorONG(id:number){
    this.adopcionesService.getAdopcionesByONG(id).subscribe( resp => {
      this.adopciones = resp.rows;
      this.calculoEdadAproximada(this.adopciones);
      console.log(this.adopciones);
    });
  }

  public filtrarPorAdoptante(id:number){
    this.adopcionesService.getAdopcionesByIdAdoptante(id).subscribe( resp => {
      this.adopciones = resp.rows;
      this.calculoEdadAproximada(this.adopciones);
    });
  }

  public filtrarPorMascota(id:number){
    this.adopcionesService.getAdopcionesByIdMascota(id).subscribe( resp => {
      this.adopciones = resp.rows;
      this.calculoEdadAproximada(this.adopciones);
    });
  }

  public filtrarPorFecha(op:number){
    const fec : Date = new Date();
    let aux : string = "";

    switch(op){
      case 1:
        fec.setDate(fec.getDate() - 7);
        aux = fec.getFullYear()+"-"+(fec.getMonth()+1)+"-"+fec.getDate();
        break;
      case 2:
        fec.setDate(0);
        aux = fec.getFullYear()+"-"+(fec.getMonth()+1)+"-"+fec.getDate();
        break;
      case 3:
        fec.setMonth(0);
        fec.setDate(0);
        aux = fec.getFullYear()+"-"+(fec.getMonth()+1)+"-"+fec.getDate();
        break;
    }

    this.adopcionesService.getAdopcionesByPeriodo(aux).subscribe( resp => {
      this.adopciones = resp.rows;
      this.calculoEdadAproximada(this.adopciones);
    });
  }

  public filtrarPorFechaYONG(op:number){
    const fec : Date = new Date();
    let aux : string = "";

    switch(op){
      case 1:
        fec.setDate(fec.getDate() - 7);
        aux = fec.getFullYear()+"-"+(fec.getMonth()+1)+"-"+fec.getDate();
        break;
      case 2:
        fec.setDate(0);
        aux = fec.getFullYear()+"-"+(fec.getMonth()+1)+"-"+fec.getDate();
        break;
      case 3:
        fec.setMonth(0);
        fec.setDate(0);
        aux = fec.getFullYear()+"-"+(fec.getMonth()+1)+"-"+fec.getDate();
        break;
    }

    this.adopcionesService.getAdopcionesByPeriodoAndONG(aux, this.admin).subscribe( resp => {
      this.adopciones = resp.rows;
      this.calculoEdadAproximada(this.adopciones);
    });
  }
  
  calcularEdadAproximada(fechaNacimiento: Date){
    var fecha1 = moment(this.fechaHoy);
    var fecha2 = moment(fechaNacimiento);

    var mesesHoy = this.pipe.transform(Date.now(), 'MM');
    var mesesNacimiento = this.pipe.transform(fechaNacimiento, 'MM');

    if(fecha1.diff(fecha2, 'years') == 0){
      this.edadAproximada = fecha1.diff(fecha2, 'months') + ' meses.';
    }else{
      if(Number(mesesHoy) < Number(mesesNacimiento)){
        let diferencia = Number(mesesNacimiento) - Number(mesesHoy);
        let meses = 12 - diferencia;
        this.edadAproximada = fecha1.diff(fecha2, 'years') + ' años y ' + meses + ' meses.';
      }else{
        let diferencia = Number(mesesHoy) - Number(mesesNacimiento);
        this.edadAproximada = fecha1.diff(fecha2, 'years') + ' años y ' + diferencia + ' meses.';
      }
    }
  }

  calculoEdadAproximada(adopciones: any){
    for(let i=0; i<adopciones.length; i++){
      this.fechaNacimiento = this.pipe.transform(adopciones[i].nacimiento, 'yyyy/MM/dd');
      this.calcularEdadAproximada(this.fechaNacimiento);
      adopciones[i].edadAproximada = this.edadAproximada;
    }
  }
}
