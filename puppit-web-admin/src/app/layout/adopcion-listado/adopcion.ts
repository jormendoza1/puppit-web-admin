export class Adopcion{
    id : number;
    fecha : Date;
    seguimiento : number;
    id_adoptante : number;
    id_mascota : number;
    id_ong : number;

    constructor(public i:number, fec:Date, seg:number, adop:number, mas:number, ong:number){
        this.id = i;
        this.fecha = fec;
        this.seguimiento = seg;
        this.id_adoptante = adop;
        this.id_mascota = mas;
        this.id_ong = ong;
    }
}