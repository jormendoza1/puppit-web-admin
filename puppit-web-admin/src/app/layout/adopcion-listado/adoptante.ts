export class Adoptante{
    id: number;
    apellido: string;
    nombre: string;
    dni: string;
    codigo_postal: string;
    direccion: string;
    email: string;
    celular: string;
    fechaNacimiento: Date;
    sexo: string;
    
    constructor(public i:number, ap:string, nom:string, d:string, cp:string, dir:string, e:string, cel:string, fnac:Date, s:string){
        this.id = i;
        this.apellido = ap;
        this.nombre = nom;
        this.dni = d;
        this.codigo_postal = cp; 
        this.direccion = dir;
        this.email = e;
        this.celular = cel;
        this.fechaNacimiento = fnac;
        this.sexo = s;
    }
}