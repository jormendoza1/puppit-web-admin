export class Mascota{
    id: number;
    name: string;
    raza: string;
    edad: number;
    tamanio: string;
    peso: any;
    sexo: string;
    nacimiento: Date;
    url_imagen: string;
    descripcion: string;
    color: string;
    id_ong: number;
    estado_adopcion: boolean;

    constructor(public i:number, name:string, raza:string, edad:number, tamanio:string, peso:any, s: string, nac:Date, url:string, des:string, col:string, ong:number, estado:boolean){
        this.id = i;
        this.name = name;
        this.raza = raza;
        this.edad = edad;
        this.tamanio = tamanio;
        this.peso = peso;
        this.sexo = s;
        this.nacimiento = nac;
        this.url_imagen = url;
        this.descripcion = des;
        this.color = col;
        this.id_ong = ong;
        this.estado_adopcion = estado;
    }
}