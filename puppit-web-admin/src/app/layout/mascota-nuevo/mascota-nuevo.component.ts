import { DatePipe, getLocaleDateTimeFormat } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdopcionesService } from 'src/app/services/adopciones.service';
import { ImageService } from 'src/app/services/image.service';
import { MascotasService } from 'src/app/services/mascotas.service';
import { OngService } from 'src/app/services/ong.service';
import { Mascota } from '../adopcion-listado/mascota';
import * as moment from 'moment/moment';

@Component({
  selector: 'app-mascota-nuevo',
  templateUrl: './mascota-nuevo.component.html',
  styleUrls: ['./mascota-nuevo.component.css']
})
export class MascotaNuevoComponent implements OnInit { 
  
  mascotas : Array<Mascota> = [];
  mascota = new Mascota(0,"","",0,"",0.0,"",new Date(),"","","",0,false);
  admin : number = 0;
  ongNombre: string = "";


  nombre = ''; 
  nacimiento = new Date();
  edad = 0;
  raza = '';
  tamanio = '';
  sexo = '';
  peso = '';
  color = '';
  imagen = '';
  descripcion = '';
  urlAuxiliar = '';
  id_ong = 0;
  estado_adopcion = false;
  mascotaSexo = '';
  modifFecha = false;
  modifImage = false;
  modalModif = false;

  pipe = new DatePipe('en-US');
  fechaHoy = this.pipe.transform(Date.now(), 'yyyy/MM/dd');
  fechaNacimiento: any;
  edadAproximada: any;

  mascotaForm: FormGroup | undefined;
 
  constructor(private formBuilder: FormBuilder, private mascotasService: MascotasService, private imageService: ImageService, private ongService: OngService) { }

  ngOnInit(): void {
    var aux = localStorage.getItem('currentUser');
    this.admin = Number(aux);

    if(this.admin==1){
      this.cargarMascotasEnAdopcion();
    }else{
      this.cargarMascotasEnAdopcionONG();
    }
  }

  // cargar mascotas en adopcion
  public cargarMascotasEnAdopcion(){
    this.mascotasService.getMascotasEnAdopcion(0).subscribe( resp => {
      this.mascotas = resp.rows;
    });
  }

  //cargar mascotas en adopcion por ong
  public cargarMascotasEnAdopcionONG(){
    this.ongService.getONGByUserId(this.admin).subscribe( resp => {
      this.id_ong=resp.rows[0].id;
      this.filtrarPorONG(resp.rows[0].id);
      this.id_ong = resp.rows[0].id;
    })
  }

  public filtrarPorONG(id:number){
    this.mascotasService.getMascotasEnAdopcionONG(id).subscribe( resp => {
      this.mascotas = resp.rows;
    });
  }

  // mostrar modal de mascota
  showModal(mascotaId: number){
    this.mascotasService.getMascotaById(mascotaId).subscribe( resp => {
      this.mascota = resp.rows[0];
      if(resp.rows[0].sexo == 'M' || resp.rows[0].sexo == 'm'){
        this.mascotaSexo = 'Macho';
      }else{
        this.mascotaSexo = 'Hembra';
      }

      this.fechaNacimiento = this.pipe.transform(this.mascota.nacimiento, 'yyyy/MM/dd');
      this.calcularEdadAproximada(this.fechaNacimiento);
    })
  }

  calcularEdadAproximada(fechaNacimiento: Date){
    var fecha1 = moment(this.fechaHoy);
    var fecha2 = moment(fechaNacimiento);

    var mesesHoy = this.pipe.transform(Date.now(), 'MM');
    var mesesNacimiento = this.pipe.transform(fechaNacimiento, 'MM');

    if(fecha1.diff(fecha2, 'years') == 0){
      this.edadAproximada = fecha1.diff(fecha2, 'months') + ' meses.';
    }else{
      if(Number(mesesHoy) < Number(mesesNacimiento)){
        let diferencia = Number(mesesNacimiento) - Number(mesesHoy);
        let meses = 12 - diferencia;
        this.edadAproximada = fecha1.diff(fecha2, 'years') + ' años y ' + meses + ' meses.';
      }else{
        let diferencia = Number(mesesHoy) - Number(mesesNacimiento);
        this.edadAproximada = fecha1.diff(fecha2, 'years') + ' años y ' + diferencia + ' meses.';
      }
    }

    this.modifFecha = false;
  }

  onInput(e: Event){
    const input = e.target as HTMLInputElement;
    if(input.files != null){
      this.imageService.upload(input.files[0]).subscribe(
        url=>{
          console.log(url);
          this.urlAuxiliar= url;
          console.log(this.urlAuxiliar);
          this.mascota.url_imagen = this.urlAuxiliar;
          this.modifImage = false;
        }
      )
    }
  }

  //MODIFICAR MASCOTA
  cargarDatosAModificar(){
    this.nombre = this.mascota.name;
    this.nacimiento = this.mascota.nacimiento;
    this.edad = this.mascota.edad;
    this.raza = this.mascota.raza;
    this.tamanio = this.mascota.tamanio;
    this.sexo = this.mascota.sexo;
    this.peso = this.mascota.peso;
    this.color = this.mascota.color;
    this.imagen = this.mascota.url_imagen;
    this.descripcion = this.mascota.descripcion;
  }

  modificar(id: any){
    this.mascotasService.editMascotas(id, this.cargarDatosMascota()).subscribe( resp => {
      this.cargarMascotasEnAdopcionONG();
    });  
    this.modalModif = true;
  }

  modificarFecha(){
    this.modifFecha = true;
  }

  modificarImagen(){
    this.modifImage = true;
  }

  cargarDatosMascota(){
    this.mascota.name = this.nombre;
    this.mascota.nacimiento = this.nacimiento;
    this.mascota.edad = this.edad;
    this.mascota.raza = this.raza;
    this.mascota.tamanio = this.tamanio;
    this.mascota.sexo = this.sexo;
    this.mascota.peso = this.peso;
    this.mascota.color = this.color;
    this.mascota.url_imagen = this.urlAuxiliar;
    this.mascota.descripcion = this.descripcion;
    this.mascota.id_ong = this.id_ong;

    return this.mascota;
  }

  //AGREGAR NUEVA MASCOTA 
  //limpiar campos
  cargarDatosNuevaMascota(){
    this.nombre = '';
    this.nombre = ''; 
    this.nacimiento = new Date();
    this.edad = 0;
    this.raza = '';
    this.tamanio = '';
    this.sexo = '';
    this.peso = '';
    this.color = '';
    this.imagen = '';
    this.descripcion = '';
  }
  
  enviar(){
    this.mascotasService.postMascotas(this.cargarDatosMascota()).subscribe( resp => {
    });  
    this.cargarMascotasEnAdopcionONG();
    this.modalModif = false;
  }

  instanciarNueva(){
    this.cargarDatosNuevaMascota();
    this.mascota = new Mascota(0,"","",0,"",0.0,"",new Date(),"","","",0,false);
  }

  
  obtenerONG(id: any){
    
    this.ongService.getONGByUserId(Number(id)).subscribe( resp => {
      this.ongNombre = resp.rows[0].nombre;
    });
    
  }

}

