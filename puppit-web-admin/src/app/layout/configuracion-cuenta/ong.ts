export class Ong{
    celular: string;
    nombre: string;
    direccion: string;
    codigo_postal: string;
    email: string;
    link_donacion: string;
    terminos_adopcion: string;
    provincia: string;


    constructor(c:string, n:string, d:string, cP:string, e:string, lD:string, tA:string, p:string){
        this.celular = c;
        this.nombre = n;
        this.direccion = d;
        this.codigo_postal = cP;
        this.email = e;
        this.link_donacion = lD;
        this.terminos_adopcion = tA;
        this.provincia = p;
    }
}