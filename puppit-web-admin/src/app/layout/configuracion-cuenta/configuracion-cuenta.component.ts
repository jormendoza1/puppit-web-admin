import { Component, OnInit } from '@angular/core';
import { OngService } from 'src/app/services/ong.service';
import { Ong } from './ong';

@Component({
  selector: 'app-configuracion-cuenta',
  templateUrl: './configuracion-cuenta.component.html',
  styleUrls: ['./configuracion-cuenta.component.css']
})
export class ConfiguracionCuentaComponent implements OnInit {

  celular = '';
  nombre = '';
  direccion = '';
  codigo_postal = '';
  email = '';
  link_donacion = '';
  terminos_adopcion = '';
  provincia = '';
  ong : Ong = new Ong('','','','','','','','');

  //recupera el id del usuario de la ong logeada.
  aux = localStorage.getItem('currentUser');
  idOng = Number(this.aux);
  
  constructor(private ongService: OngService) { }

  ngOnInit(): void {
     this.cargarDatos();
  }

  cargarDatos(){
    this.ongService.getONGByUserId(this.idOng).subscribe( resp => {
      this.ong = resp.rows;

      this.celular = this.ong[0].celular;
      this.nombre = this.ong[0].nombre;
      this.link_donacion = this.ong[0].link_donacion;

      this.direccion = this.ong[0].direccion;
      this.codigo_postal = this.ong[0].codigo_postal;
      this.email = this.ong[0].email;
      this.provincia = this.ong[0].provincia;

      this.terminos_adopcion = this.ong[0].terminos_adopcion;
    });
  }

  enviar(){
    let ong = new Ong(this.celular, this.nombre, this.direccion, this.codigo_postal, this.email, this.link_donacion, this.terminos_adopcion, this.provincia);
    this.ongService.editONG(this.idOng, ong).subscribe(resp => {
      console.log(resp);
    })
  }
}
