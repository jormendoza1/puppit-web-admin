import { Component, OnInit } from '@angular/core';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email : string = "";
  password : string = "";
  error : boolean = false;
  mensajeError : string = "";

  constructor(private usuariosService: UsuariosService) { }

  ngOnInit(): void {
  }

  public ingresar(){

    
      this.usuariosService.getUsuario(this.email, this.password).subscribe( resp => {
        if(resp.ok){
  
          localStorage.setItem('currentUser', JSON.stringify(resp.resp.id));
  
            if(resp.resp.id == 1){
              window.location.href="http://localhost:4200/mascota";
            }else{
              window.location.href="http://localhost:4200/mascota";
            }
             
        }else{
          this.mensajeError = resp.error;
          this.error = true;
        }
      });
    
  }
}