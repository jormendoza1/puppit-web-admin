import { Component, OnInit } from '@angular/core';
import { OngService } from 'src/app/services/ong.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  ongNombre : string = "";
  admin : number = 0;
  constructor(private ongService : OngService) { }

  ngOnInit(): void {
    var aux = localStorage.getItem('currentUser');
    this.admin = Number(aux);

    if(Number(aux)==1){
      this.ongNombre = "Administrador";
    }else{
      this.ongService.getONGByUserId(Number(aux)).subscribe( resp => {
        this.ongNombre = resp.rows[0].nombre;
      });
    }
  }

  public cerrarSesion(){
    localStorage.removeItem('currentUser');
    window.location.href="http://localhost:4200";
  }
}
