import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const cudOptions = {
  headers: new HttpHeaders ({'Content-Type':'application/json'}), 
}

@Injectable({
  providedIn: 'root'
})
export class AdoptantesService {

  private urlBase = environment.url_servicios_base;
  private apiAdoptantes = this.urlBase + '/api/adoptantes';

  constructor(public http: HttpClient) {}

  getAdoptanteById(id:any): Observable<any> {
    return this.http.get<any[]>(this.apiAdoptantes+"/"+id, cudOptions)
  }

  getAdoptantesByONG(id:any): Observable<any> {
    return this.http.get<any[]>(this.apiAdoptantes+"/adopcion-ong/"+id, cudOptions)
  }
}
