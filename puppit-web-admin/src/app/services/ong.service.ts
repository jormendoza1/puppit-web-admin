import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const cudOptions = {
  headers: new HttpHeaders ({'Content-Type':'application/json'}), 
}

@Injectable({
  providedIn: 'root'
})
export class OngService {

  private urlBase = environment.url_servicios_base;
  private apiONG = this.urlBase + '/api/ong/';

  constructor(public http: HttpClient) {}

  getONGs(): Observable<any> {
    return this.http.get<any[]>(this.apiONG, cudOptions)
  }

  getONGByUserId(id:any): Observable<any> {
    return this.http.get<any[]>(this.apiONG+'/user/'+id, cudOptions)
  }

  editONG(id: any, ong: any): Observable<any> {
    return this.http.put<any[]>(this.apiONG+id, ong, cudOptions)
  }
}
