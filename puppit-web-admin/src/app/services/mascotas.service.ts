import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const cudOptions = {
    headers: new HttpHeaders ({'Content-Type':'application/json'}), 
}

@Injectable({
  providedIn: 'root'
})
export class MascotasService {

  private urlBase = environment.url_servicios_base;
  private apiMascotas = this.urlBase + '/';
  
  constructor(public http: HttpClient) {}

    postMascotas(mascota: any): Observable<any> {
      const newSession = Object.assign({}, mascota);
      return this.http.post<any[]>(this.apiMascotas, newSession, cudOptions)
   }

    getMascotaById(id: any): Observable<any> {
      return this.http.get<any[]>(this.apiMascotas+id, cudOptions)
    }

    getMascotasAdoptadasByONG(id:any): Observable<any> {
      return this.http.get<any[]>(this.apiMascotas+"mascotas/adoptadas/"+id, cudOptions)
    }

    getMascotasEnAdopcion(estado_adopcion: any): Observable<any> {
      return this.http.get<any[]>(this.apiMascotas+"estado/"+estado_adopcion, cudOptions)
    }

    getMascotasEnAdopcionONG(id: any): Observable<any> {
      return this.http.get<any[]>(this.apiMascotas+"ong/adopciones/"+id, cudOptions)
    }

    editMascotas(id:any, mascota: any): Observable<any> {
      console.log(id, mascota)
      return this.http.put<any[]>(this.apiMascotas+'modificar/'+id, mascota, cudOptions)
    }
}
