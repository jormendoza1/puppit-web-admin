import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const cudOptions = {
    headers: new HttpHeaders ({'Content-Type':'application/json'}), 
}

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  //acá viene la apiKey de cada uno, yo puse la mia en este caso.
  private readonly apiKey: string = '538a39b4b4e0ce4b1f254e9e796803b9';
  
  constructor(public httpCliente: HttpClient) {}
  

  upload(file: File): Observable<any>{
    const formData = new FormData();
    formData.append('image', file);

    return this.httpCliente
      .post('/upload', formData, {params: {key: this.apiKey}})
      .pipe(map((response)=> response['data']['url']));
  }
}
