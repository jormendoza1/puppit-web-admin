import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const cudOptions = {
  headers: new HttpHeaders ({'Content-Type':'application/json'}), 
}

@Injectable({
  providedIn: 'root'
})
export class AdopcionesService {

  private urlBase = environment.url_servicios_base;
  private apiAdopciones = this.urlBase + '/api/adopciones'; 

  constructor(public http: HttpClient) {}

  getAdopciones(): Observable<any> {
    return this.http.get<any[]>(this.apiAdopciones, cudOptions)
  }

  getAdopcionById(id:any): Observable<any> {
    return this.http.get<any[]>(this.apiAdopciones+'/'+id, cudOptions)
  }

  getAdopcionesByONG(id:any): Observable<any> {
    return this.http.get<any[]>(this.apiAdopciones+"/ong/"+id, cudOptions)
  }

  getAdopcionesByIdAdoptante(id:any): Observable<any> {
    return this.http.get<any[]>(this.apiAdopciones+"/filtro/adoptante/"+id, cudOptions)
  }

  getAdopcionesByIdMascota(id:any): Observable<any> {
    return this.http.get<any[]>(this.apiAdopciones+"/filtro/mascota/"+id, cudOptions)
  }

  getAdopcionesByPeriodo(fec:any): Observable<any> {
    return this.http.get<any[]>(this.apiAdopciones+"/filtro/fecha/"+fec, cudOptions)
  }

  getAdopcionesByPeriodoAndONG(fec:any, idONG:any): Observable<any> {
    return this.http.get<any[]>(this.apiAdopciones+"/filtro/fecha/"+fec+"/"+idONG, cudOptions)
  }
}
