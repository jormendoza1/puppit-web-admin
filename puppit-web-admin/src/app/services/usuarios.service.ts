import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const cudOptions = {
  headers: new HttpHeaders ({'Content-Type':'application/json'}), 
}

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  private urlBase = environment.url_servicios_base;
  private apiUsuario = this.urlBase + '/api/usuario';

  constructor(public http: HttpClient) {}

  getUsuario(email:any, pwd:any): Observable<any> {
    return this.http.get<any[]>(this.apiUsuario+'/'+email+'/'+pwd, cudOptions)
  }
}
